# Base information: image, env variables and working directory.
FROM netboxcommunity/netbox:latest

ADD custom-auth/auth.py /opt/netbox/netbox/utilities/auth_backends.py
ADD custom-auth/login.html /opt/netbox/netbox/templates/login.html

EXPOSE 8001