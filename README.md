## Pass-Through SSO for Netbox
If you are running netbox in Kubernetes and want to use either GitHub, GitLab, Azure AD or any other Oauth2 compatible provider to authenticate your applications, you might want to do so for Netbox too, this project builds on the basics of the Django Authentication framework to provide such a scenario.

This project has been tested using the ```pusher/oauth2_proxy``` image, but should work with all other variants.

#### Requirements
Environment Variables

 - ```PREAUTH_COOKIE```: Name of the auth proxy cookie.
 - ```BASE64_ENCODED```: If set to **True**, the cookie will be base64 decoded.
 
 Images are built daily and updated with their unique image tag, meaning you can use spinnaker or other providers to monitor for changes in the registry. All newly built images are also tagged as ```latest``` for convenience.

#### Examples
There are examples for both the deployment and ingress components for kubernetes in the /kubernetes sub-directory.

#### Not using Docker?
Copy the following files to the respective directories:
- ```custom-auth/auth.py``` to ```/opt/netbox/netbox/utilities/auth_backends.py```
- ```custom-auth/login.html``` to ```/opt/netbox/netbox/templates/login.html```
Then add the Evironment variables above to your system.

(This assumes netbox is installed in the ```/opt/netbox``` directory.)

