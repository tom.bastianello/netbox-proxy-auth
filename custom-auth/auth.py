# Generic imports.
import os
import base64
import re

# Django imports.
import logging

from django.conf import settings
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User

logger = logging.getLogger(__name__)

class ViewExemptModelBackend:
    def authenticate(self, request, username=None, password=None):
        cookie_username = None
        if os.environ['PREAUTH_COOKIE']:
            cookie_username = request.COOKIES.get(os.environ['PREAUTH_COOKIE'])
        
        base64decode = True
        if os.environ['BASE64_ENCODED']:
            base64decode = os.environ['BASE64_ENCODED']

        logger.info('Performing authentication for ' + username + '')
        
        if cookie_username:
            cookie_username = (cookie_username.split("|")[0]).encode("utf-8")
            if base64decode:
                cookie_username = base64.b64decode(cookie_username).decode("utf-8") 
            cookie_username = cookie_username.lower()
            emails = re.findall("([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)", cookie_username)
            cookie_username = emails[0]
        
        username = cookie_username

        try:
            user = User.objects.get(username=username)
            return user
        except:
            logger.error('User ' + username + ' does not exist, creating...')
            user = User(username=username, password="sso")
            user.is_staff = False
            user.is_superuser = False
            user.save()
            logger.info('User ' + username + ' created!')
            return user
        
        return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except:
            logger.error('Error getting user (' + str(user_id) + ')')
            return None

    def has_perm(self, user_obj, perm, obj=None):
        # If this is a view permission, check whether the model has been exempted from enforcement
        app, codename = perm.split('.')
        action, model = codename.split('_')

        if action in user_obj.user_permissions or action == 'view':
            return True
        else:
            return False